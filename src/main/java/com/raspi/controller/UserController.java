package com.raspi.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.raspi.model.Password;
import com.raspi.model.User;
import com.raspi.repository.UserRepository;

@Controller
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;  
	
	@Autowired
	private UserRepository userRepository;
	
	
	@RequestMapping(value = "/userdetails", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
    @ResponseBody
    public String getUserInfo() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		String username;
		
		if (principal instanceof UserDetails) {

		  username = ((UserDetails)principal).getUsername();

		} else {
			
		  username = principal.toString();

		}
		
		User u = userRepository.findByUsername(username);
		
		Map<String,String> myMap = new HashMap<String, String>();
	    myMap.put("nome", u.getFirstName());
	    myMap.put("cognome", u.getLastName());
	    myMap.put("email", u.getEmail());
	    ObjectMapper mapper = new ObjectMapper();
	    String json = "";
	    try {
	        json = mapper.writeValueAsString(myMap);
	    } catch (JsonProcessingException e) {
	        // TODO Auto-generated catch block
	       e.printStackTrace();
	    }
	    
	    return json;
	}
    
	@GetMapping("/profile")
    public String getUserDetails(Model model) {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = "";
		
		if (principal instanceof UserDetails) {
		  username = ((UserDetails)principal).getUsername();
		} else {
		  username = principal.toString();
		}
		
		User u = userRepository.findByUsername(username);
		model.addAttribute("user", u);
        return "profile";
    }
	
	@PostMapping("/update")
	public String update(User user) {
		System.out.println(user);
		User u = userRepository.findByEmail(user.getEmail());
		System.out.println(u);

		String updateQuery = "update user set first_name = ?, last_name = ? where id = ?";
		jdbcTemplate.update(updateQuery, user.getFirstName(), user.getLastName(), u.getId());
		
//		userRepository.update(user.getFirstName(), user.getLastName(), user.getEmail(), u.getId());;
		return "redirect:/user/profile?success";
	}
	
	 @GetMapping("changePassword")
	 public String changePassword(Model model, @ModelAttribute("error") final String mapping1FormObject) {
		 model.addAttribute("password", new Password());
		 if(mapping1FormObject.length()>0) {
			 model.addAttribute("motivo", mapping1FormObject);
		 }
		 return "changePassword";
	 }
	
	@PostMapping("changePassword")
	public String changePassword(@ModelAttribute("password") @Valid Password password, BindingResult result, RedirectAttributes redirectAttributes) {
		System.out.println(password);
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = ((UserDetails)principal).getUsername();
		User u = userRepository.findByUsername(username);
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		
		//password sbagliata
		if(passwordEncoder.matches(password.getPassword(), u.getPassword())) {
			//password coincidono
			if(password.getNewPassword().equals(password.getRepeatPassword())) {
				//vecchia password uguale a nuova
				String hashedPassword = passwordEncoder.encode(password.getNewPassword());
				if(passwordEncoder.matches(password.getPassword(), hashedPassword)) {
					redirectAttributes.addFlashAttribute("error", "La nuova password è uguale a quella vecchia");
					result.rejectValue("password", null, "La nuova password è uguale a quella vecchia");
				} else {
					String updateQuery = "update user set password = ? where id = ?";
					jdbcTemplate.update(updateQuery, hashedPassword, u.getId());
				}
			} else {
				redirectAttributes.addFlashAttribute("error", "Le due password non coincidono");
				result.rejectValue("password", null, "Le due password non coincidono");
			}
//			redirectAttributes.addFlashAttribute("error", "Password aggiornata con successo");
		} else {
			redirectAttributes.addFlashAttribute("error", "La password corrente è errata");
			result.rejectValue("password", null, "La password corrente è errata");
		}
		
		if (result.hasErrors()) {
			return "redirect:/user/changePassword?error";
		}
		
		return "redirect:/user/changePassword?success";
	}
	
}
