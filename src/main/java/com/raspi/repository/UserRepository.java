package com.raspi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.raspi.model.User;

@Repository
public interface UserRepository extends JpaRepository <User, Long>{
	
	User findByEmail(String email);
	
	User findByUsername(String username);
	
	@Modifying
	@Query("UPDATE User u SET u.firstName = :firstName, u.lastName = :lastName, u.email = :email WHERE u.id = :id")
	void update(String firstName, String lastName, String email, long id);
	
}