package com.raspi.model;

import javax.validation.constraints.Size;

public class Password {

	private String password;
	@Size(min = 3)
	private String newPassword;
	private String repeatPassword;
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getRepeatPassword() {
		return repeatPassword;
	}
	public void setRepeatPassword(String repeatPassword) {
		this.repeatPassword = repeatPassword;
	}
	
	@Override
    public String toString() {
        return "Password{" +
            "password=" + password +
            ", newPassword='" + newPassword + '\'' +
            ", repeatPassword=" + repeatPassword +
            '}';
    }
	
}
