package com.raspi.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.raspi.dto.UserRegistrationDto;
import com.raspi.model.User;

public interface UserService extends UserDetailsService {
	
	User findByEmail(String email);
	
	User findByUsername(String username);

    User save(UserRegistrationDto registration);
}
