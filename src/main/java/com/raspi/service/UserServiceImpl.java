package com.raspi.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.raspi.dto.UserRegistrationDto;
import com.raspi.model.Role;
import com.raspi.model.User;
import com.raspi.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
    private BCryptPasswordEncoder passwordEncoder;


	public User save(UserRegistrationDto registration) {
		User user = new User();
	    user.setUsername(registration.getUsername());
	    user.setFirstName(registration.getFirstName());
	    user.setLastName(registration.getLastName());
	    user.setEmail(registration.getEmail());
	    user.setPassword(passwordEncoder.encode(registration.getPassword()));
	    user.setRoles(Arrays.asList(new Role("ROLE_USER")));
	    return userRepository.save(user);
	}
	 
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user_email = userRepository.findByEmail(username);
		User user_username = userRepository.findByUsername(username);
		
        if (user_username == null && user_email == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        
        User user = user_email == null ? user_username : user_email;
        
        return new org.springframework.security.core.userdetails.User(user.getUsername(),
            user.getPassword(),
            mapRolesToAuthorities(user.getRoles()));
	}

	@Override
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}
	
	private Collection < ? extends GrantedAuthority > mapRolesToAuthorities(Collection < Role > roles) {
        return roles.stream()
            .map(role -> new SimpleGrantedAuthority(role.getName()))
            .collect(Collectors.toList());
    }

	@Override
	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

}
