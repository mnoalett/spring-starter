var application = angular.module('app', []);

application.controller('appController', function ($scope, $http) {
	console.log("hello")
	
	$scope.updated = false;
	
	$scope.user = {};
	
	$http.get('/user/userdetails').then(function(response){
		console.log(response);
		var data = response.data;
		$scope.user = data
	}, function(error){
		console.log(error);
	})
	
	
	 $scope.updateUser = function() {
		console.log($scope.user);
		$scope.updated = false;
		$http.post('/user/updateProfile', $scope.user).then(function(response){
			console.log("user updated");
			$scope.updated = true;
			var data = response.data;
		}, function(error){
			console.log(error);
		})
		
     }
});

